all: programmatically

programmatically: programmatically.m Chrome.h
	clang -Wall -Wextra -Og -g3 -glldb -fdebug-macro -fmodules -o $@ $<

Chrome.h:
		sdef '/Applications/Google Chrome.app' | sdp -fh --basename $(@F)

%.dylib: %.c
	clang -Wall -Wextra -dynamiclib -g3 -glldb -fdebug-macro -o $@ $<
