#include <dlfcn.h>
#include <xpc/xpc.h>
#include <Carbon/Carbon.h>

/*
mach_msg_return_t (*orig_mach_msg)(mach_msg_header_t *,
        mach_msg_option_t, mach_msg_size_t,
        mach_msg_size_t,
        mach_port_t,
        mach_msg_timeout_t,
        mach_port_t);

mach_msg_return_t mach_msg(mach_msg_header_t *msg,
        mach_msg_option_t option,
        mach_msg_size_t send_size,
        mach_msg_size_t rcv_size,
        mach_port_t rcv_name,
        mach_msg_timeout_t timeout,
        mach_port_t notify){

    if(!orig_mach_msg){
        orig_mach_msg = dlsym(RTLD_NEXT, "mach_msg");
    }

    mach_msg_return_t ret = orig_mach_msg(msg,
            option,
            send_size,
            rcv_size,
            rcv_name,
            timeout,
            notify);

    return(ret);
}

xpc_object_t (*orig_xpc_connection_send_message_with_reply_sync)(xpc_connection_t connection,
	xpc_object_t message);

xpc_object_t
xpc_connection_send_message_with_reply_sync(xpc_connection_t connection,
	xpc_object_t message) {
    if(!orig_xpc_connection_send_message_with_reply_sync){
        orig_xpc_connection_send_message_with_reply_sync = dlsym(RTLD_NEXT, "xpc_connection_send_message_with_reply_sync");
    }
    return orig_xpc_connection_send_message_with_reply_sync(connection, message);
  }

void
xpc_connection_send_message(xpc_connection_t connection, xpc_object_t message);

void
xpc_connection_send_message_with_reply(xpc_connection_t connection,
	xpc_object_t message, dispatch_queue_t _Nullable replyq,
	xpc_handler_t handler);
*/

OSErr (*orig_AESend)(
  const AppleEvent *  theAppleEvent,
  AppleEvent *        reply,
  AESendMode          sendMode,
  AESendPriority      sendPriority,
  SInt32              timeOutInTicks,
  AEIdleUPP           idleProc,             /* can be NULL */
  AEFilterUPP         filterProc);

OSErr
AESend(
  const AppleEvent *  theAppleEvent,
  AppleEvent *        reply,
  AESendMode          sendMode,
  AESendPriority      sendPriority,
  SInt32              timeOutInTicks,
  AEIdleUPP           idleProc,             /* can be NULL */
  AEFilterUPP         filterProc) {
    if(!orig_AESend){
        orig_AESend = dlsym(RTLD_NEXT, "AESend");
    }
    return orig_AESend(theAppleEvent, reply, sendMode, sendPriority, timeOutInTicks, idleProc, filterProc);
}
