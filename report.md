# AppleScript internals: from AppleScripts to Mach messages

The following introduction to AppleScript internals is based on the different
documentations listed in the bibliography, and findings made during the
debugging of the sample AppleScript.

The AppleScript API is the standard macOS API for inter-process communication
(IPC). When a macOS application needs to expose an API to other processes, this
is macOS' standard way of doing it. Of course, any other IPC method on macOS
(which is a UNIX operating-system) would be valid, but they only provide
inter-process data transport while AppleScript also specifies and provides the
inter-process message structures.

When a user runs an AppleScript script, script statements that target an
application are converted into corresponding Apple Event descriptors sent to the
application. For each Apple Event descriptor received that corresponds to a
command defined in the Scripting Definition File (sdef) of the application,
Cocoa scripting instantiates in the application a script command object that
contains all the information needed to describe the specified operation and
perform it. It then executes the script command, which works with the actual
objects in the application to perform the received operation. The Apple Event
flow is bidirectional and script commands can return values to the originating
program.

Have a look at [Google Chrome's Scripting Definition File][chrome-sdef] to see
what objects it exposes and how it describes their relationships. In the end,
the information it contains will be used in the Apple Event descriptors to
target specific objects and properties in the "object graph" defined in the
SDEF. For more on Scripting Definition Files, see [Preparing a Scripting
Definition File][sdef].

Apple Event descriptors provide a data transport and event dispatching
mechanism. On sender's side, they are used as abstract representation of an
AppleScript script and transport description to specify a receiver. The
resulting descriptor is sent to another process using inter-process
communication (the XPC API). On receiver's side, the response (if any) is also
an Apple Event descriptor. Any linear data structure (a data structure not
requiring pointers) can be represented as an Apple Event descriptor.

For the request, the abstract representation of the AppleScript is sent as
nested structures of directives. For example, the expression to `get the URL of
all tabs of all windows` is represented as following (it is simplified to make it less mysterious...):

```
{ // the root descriptor specifies the "entry point"
  core,getd // core suite, getd command
  target = bundle("com.google.Chrome"),
  { // Payload, called "direct object"
    ---- = { // read URL property
      form = prop,
      want = prop,
      seld = URL , // note the trailing space, because everything is 4-char (32 bits)
      from = { // from the indexing of all tabs
        form = indx,
        want = CrTb,
        seld = all,
        from = { // from the indexing of all windows
          form = indx,
          want = cwin,
          seld = all,
          from = NULL
        }
      }
    }
  }
}
```

This Apple Event descriptor is made of nested sub-descriptors and clearly shows
how AppleScript scripts can be expressed as Apple Event descriptors. The example
here expresses the different AppleScript directives of our script: (i) `tell
Google Chrome to get`; (ii) `URL`; (iii) `of all tabs`; (iv) `of all windows`.
The overall object consists of a set of AppleScript directives (field `form`)
and their parameters (fields `want` and `seld`) to address application objects
and properties defined in its SDEF. It is executed by the target application's
Main thread once received and validated. You can observe how this structure
follows the [Chrome's SDEF][chrome-sdef] and how it allows perform the actual
operation on Chrome's objects.

Regarding this pretty weird output, the best reference explaining it is
[AppleScript: The Definitive Guide][applescript-book]. But know that this `----`
key means "direct object", i.e. the actual data of the root descriptor (you will
find back this name in the source code).

For an application to handle a specific Apple Event such as ours, it must
register with the Apple Event Manager a function that handles events of that
type. The handler extracts pertinent data from an Apple event, performs the
requested action, and if necessary, returns a result in the form of an Apple
Event descriptor. To send an Apple Event descriptor, you have to specify
information such as the target application to send the Apple event to, a
timeout, and whether to allow interaction with the user (for example, if the
Apple event might result in showing a dialog). For more general details, see
[Cocoa Event Architecture][coco-event-arch].

The underlying implementation of Apple Event is then pretty straightforward: it
is based on the XPC API, which is based on kernel's IPC mechanism (Mach
messages).

# Debugging services of the AppleScript API

Some conclusions on how to debug (i.e. observe) the AppleScript stack.

## Using LLDB

Using a debugger is of course the best option since it allows to dynamically
observe everything in a program. But we are willing to debug the macOS
libraries, and they are not distributed with their debugging symbols. In the
absence of debugging symbols, the only debugging option we have is using the
assembly view, which has the poorest debugging experience whereas we would like
to print function arguments with their structure (which requires having the
debugging information of involved structures).

So the idea to make source-level debugging possible is inserting debugging
symbols of functions we want to observe by redefining them in a library (calling
back their actual implementation), and by compiling it as a dynamic library with
full debugging options. This dynamic library can then be inserted in the loading
of the program using `DYLD_*` environment variables.

Of course, this is not a full source-level debugging because we don't have the
source-code of macOS' libraries, but at least we can insert breakpoints in our
hooked functions and examine its arguments and the backtrace.

The compilation command is stored in the package's Makefile:
```console
$ make hook.dylib
clang -Wall -Wextra -dynamiclib -g3 -glldb -fdebug-macro -o myhook.dylib myhook.c

```

Then run LLDB and set the DYLD environment variables to preload our library:

```console
$ lldb osascript urls.applescript
(lldb) settings set target.env-vars DYLD_INSERT_LIBRARIES=hook.dylib
(lldb) settings set target.env-vars DYLD_FORCE_FLAT_NAMESPACE=1
(lldb) br your_hooked_function
(lldb) run
...
```

It will stop `osascript` in the hooked function, able to do fully-featured
source-level debugging of the hooked function.

## Using debugging environment variables

### AppleScript Layer

The environment variable `NSScriptingDebugLogLevel` allows to print AppleScript
objects sent and received:

```console
$ defaults write NSGlobalDomain NSScriptingDebugLogLevel 1
$ osascript urls.applescript
Command: Standard Suite.get
	Direct Parameter: <NSPropertySpecifier: URL of tabs of appleScriptWindows>
	Receivers: <NSPropertySpecifier: URL of tabs of appleScriptWindows>
	Arguments:     {
  }

  Result: <NSAppleEventDescriptor: [ [
  'utxt'("https://mail.google.com/mail/u/0/"),
  'utxt'("https://web.whatsapp.com/"),
  'utxt'("https://github.com/rsta2/circle/pull/70#issuecomment-385914242"),
  'utxt'("https://insights.stackoverflow.com/survey/2018/")
  ...
  ] ]>
```

### AppleEvent Layer

`AEDebugSends` and `AEDebugReceives` enables printing Apple Event descriptors in
the macOS library:

```console
$ export AEDebugSends=1
$ export AEDebugReceives=1
$ osascript urls.applescript
    {core, getd target='psn '[Google Chrome] {----={form=prop, want=prop, seld=URL , from={form=indx, want=CrTb, seld=abso(4/$206c6c61), from={form=indx, want=cwin, seld=abso(4/$206c6c61), from=NULL-impl}}}} attr:{csig=65536 returnID=-18093}
...
```

This output gives the exact layout of Apple Event descriptors. Reproducing it
programmatically allows to perform the same AppleScript. The output is explained
in [AppleScript: The Definitive Guide][applescript-book] (you can use the
keyword search if you want to avoid buying it).

Note that LLDB gives more details using command `po` on the descriptor, as it
shows the type of each object (useful when having to reimplement it). 


## Using Instruments

The macOS tool `Instruments` allows to profile the execution of your program
with lots of details such as the time spent in system calls or the execution log
including the state changes of the threads with the amount of time spent before
being rescheduled.

To perform such a trace, launch the tool and create a `System Trace` profiling,
select the target program and modify the record options (`File/Record Options`)
to `Deferred` instead of the last 5 seconds, and you are finally ready to go.
Note that it is better to reduce the number of benchmark iterations, as the
resulting profiling file is big (e.g. 250 Mo with 1000 iterations).

You can find the Apple Event IPCs in the narrative logs of the Main thread, by
looking for "Google Chrome" (Cmd+F) to find clear scheduling profiling, such as:

```
00:00.343.337	Blocked for 2.87 ms (99.2% of mach_msg_trap's duration) starting at priority 31	mach_msg_trap
00:00.346.207	The thread was made runnable by Google Chrome (pid: 93608, tid: 0x11efa0) running on CPU 1.  It waited for an available CPU for 16.32 µs (0.6% of mach_msg_trap's duration) before running again at a priority of 31.	mach_msg_trap
```

This example shows an IPC round-trip of 2.87 ms (time blocked) + 16.32 µs (time
runnable).


# Converting an AppleScript into C/Objective-C

The studied sample is the following AppleScrit:

```
tell "Google Chrome" to get URL of tabs of windows
```

Three different methods have been found and will be compared. Launching
`osascript` to run an AppleScript is not part of the benchmark since its
programmatic equivalent has been used instead (using the AppleScript API).

## Using NSAppleScript

This method is implemented in function `UsingAppleScriptAPI()` of file
`programmatically.m`.

Using this API is the same as calling `osascript` but with finer-grained
controls since we are able to reuse the compiled script. This will have a
significant positive effect on the performance of the subsequent IPCs that will
be able to go much (much) faster by reusing the objects.

Here is the backtrace of an IPC when using the AppleScript API:

```text
0 libsystem_kernel.dylib mach_msg_trap
1 libsystem_kernel.dylib mach_msg
2 CoreFoundation __CFRunLoopServiceMachPort
3 CoreFoundation __CFRunLoopRun
4 CoreFoundation CFRunLoopRunSpecific
5 AE waitForReply(unsigned int, WaitForReplyElem*, unsigned int, unsigned int)
6 AE AESendMessage
7 AE aeSend
8 AppleScript ComponentSend(AEDesc const*, AEDesc*, int, int)
9 AppleScript TUASApplication::Send(TStackFrame_UASRemoteSend*, AEDesc*, AEDesc*, unsigned char, unsigned char, unsigned char)
10 AppleScript UASRemoteSend(unsigned char, unsigned char, unsigned char, unsigned char, unsigned char, unsigned char*)
11 AppleScript UASRemoteGetData(TUASObjectAlias*, TUASApplication*, TUASClassIdentifier*, unsigned char*)
12 AppleScript UASGetDataNoCircularities(unsigned char, TUASClassIdentifier*)
13 AppleScript UASGetData(unsigned char, TUASClassIdentifier*)
14 AppleScript UASExecute1()
15 AppleScript UASExecute(unsigned char)
16 AppleScript ASExecute(unsigned int, unsigned int, int, unsigned int*)
17 AppleScript AppleScriptComponent
18 AppleScript AGenericCall::Delegate(ComponentInstanceRecord*)
19 AppleScript AGenericManager::HandleOSACall(ComponentParameters*)
20 AppleScript GenericComponent
21 OpenScripting OSAExecute
22 Foundation -[NSAppleScript(NSPrivate) _executeWithMode:andReturnError:]
23 programmatically UsingAppleScriptAPI /Users/julio/Desktop/macos-rpc/programmatically.m:52
```

## Using Scripting Brigde

This method is implemented in function `UsingScriptingBridgeAPI()` of file
`programmatically.m`.

It is perfectly explained in the documentation [Using Scripting
Bridge][scripting-bridge]. It describes how to generate a header file with the
classes of an SDEF. It abstracts what we will do in the following method using
raw Apple Events. This is the most convenient way of programmatically writing
AppleScript calls.

Here is the backtrace of an IPC when using the Scripting Bridge API: 

```text
0 libsystem_kernel.dylib mach_msg_trap
1 libsystem_kernel.dylib mach_msg
2 CoreFoundation __CFRunLoopServiceMachPort
3 CoreFoundation __CFRunLoopRun
4 CoreFoundation CFRunLoopRunSpecific
5 AE waitForReply(unsigned int, WaitForReplyElem*, unsigned int, unsigned int)
6 AE AESendMessage
7 ScriptingBridge -[SBAppContext sendEvent:error:]
8 ScriptingBridge -[SBObject sendEvent:id:parameters:]
9 programmatically UsingScriptingBridgeAPI /Users/julio/Desktop/macos-rpc/programmatically.m:63
```


## Using raw Apple Event

This method is implemented in function `UsingAppleEventAPI()` of file
`programmatically.m`.

To reproduce it programmatically, dump the Apple Event descriptor using LLDB or
using `AEDebugSend` (faster to set up), and reproduce the printed structure
using the class `NSAppleEventDescriptor` and function `CreateObjSpecifier()`.

Here is the backtrace of an IPC when using raw Apple Event descriptors:

```text
0 libsystem_kernel.dylib mach_msg_trap
1 libsystem_kernel.dylib mach_msg
2 CoreFoundation __CFRunLoopServiceMachPort
3 CoreFoundation __CFRunLoopRun
4 CoreFoundation CFRunLoopRunSpecific
5 AE waitForReply(unsigned int, WaitForReplyElem*, unsigned int, unsigned int)
6 AE AESendMessage
7 Foundation -[NSAppleEventDescriptor sendEventWithOptions:timeout:error:]
8 programmatically UsingAppleEventAPI /Users/julio/Desktop/macos-rpc/programmatically.m:165
```


# Benchmark: AppleScript vs Scripting Bridge vs AppleEvent

To compile the benchmark, run the Makefile:

```console
 $ make
 sdef '/Applications/Google Chrome.app' | sdp -fh --basename Chrome.h
 clang -Wall -Wextra -O3 -g3 -glldb -fdebug-macro -fmodules -o programmatically programmatically.m
 ```

It generates the `Chrome.h` header file required by the Scripting Bridge API and
compiles `programmatically.m` into `programmatically`. Run it to get the results
of each AppleScript method. The macro `BENCHMARK_ITERATIONS` defines the number
of benchmark iterations per method.

## Results

The following results were obtained with:

- macOS High Sierra v10.13.4 (17E202)
- 2,6 GHz Intel Core i5 (MacBook Pro 13" late 2013)
- 16 Go 1600 MHz DDR3

They show the times taken to perform the overall IPC of each method once their
objects are initialized and ready to be used to perform the IPC. The following
results were made running each method 10,000 times.

| API             | Min (ns)   | Average (ns)  | Max (ns)    |
| -----------     | ---------: | ------------: | ----------: |
| AppleEvent      | 2,819,000  | 3,093,245     | 6,621,000   |
| ScriptingBridge | 3,551,000  | 3,913,447     | 10,162,000  |
| AppleScript     | 4,540,000  | 5,772,492     | 57,734,000  |

The number of samples allows a rather stable average upon several launches. The
minimum and maximum time is pretty unstable but the order is always the same.

Note that the maximum value when using AppleScript is always reached with the
first run because it compiles the script, while its following calls fall to 5,7
ms in average since it reuses its objects (but it seems to still recreate the
Apple event descriptor).

## Analysis

This overall result include:

1. The time to perform the `mach_msg()` syscall and block the thread.

1. The time for Chrome's Main thread to handle the event and perform the
   AppleScript operation.

1. The time to re-schedule our Main thread thanks to Chrome's answer.

Using the macOS standard tool `Instruments` as previously explained allows to
get some data:

- The call to `mach_msg_trap()` takes approximately 3 ms to be unblocked by
  Google Chrome's answer.

- The average time Chrome's main thread is running (most-likely to handle our
  Apple event) is approximately 300 µs (source: Thread State Trace/Thread by
  State/Running).

So it means that most of the execution time is spent context-switching
(miliseconds) while the rest of it is the processing of the AppleScript
(microseconds).

# Going lower-level?

For now, going lower-level in the stack is not necessary:

- The Scripting Bridge API or the AppleEvent API exactly provides the required
  features to create the AppleEvent.
- The AppleEvent API is a thin wrapper of the XPC library.
- They don't waste CPU time by sleeping while it waits for the reply.

Which means that going lower-level would only avoid some function calls while we
would have to reimplement their features to create our Apple Event descriptors.
Going lower-level would only be necessary to get finer-grained control flow, for
example to get better priorities. So far, this is not necessary for the purpose
of accelerating the IPC.

# Conclusion

## Results

Calling the AppleScript from its language form using `osascript` takes at least
58 ms (corresponding in the maximum value of the AppleScript results above) But
since the process quits right after printing the result, it can't make benefit
from the Apple Event descriptor it just created to reuse it in subsequent calls
(assuming it's the same script).

Using any of method offers far better performances, up to 15x faster.

Going even faster would require optimizing the context-switch time by increasing
the priorities. But 3 to 6 ms of execution is a rather sound execution time of
such IPC.

## Going event-based

The AppleScript API and the underlying Apple Event API only allow sending
messages to invoke commands and get their results (there is also an asynchronous
message passing to get the result later and avoid blocking the thread). So the
only event in this model is for the receiver, not the sender.

Getting an event from the target application when a new URL is opened would
require it to dispatch an event or send a message in a software bus or log it.

The following parts discuss some possible workarounds.

### Sampling the polling

Avoid loop-calling indefinitely the IPC. Firstly, it runs faster than a human
can possibly interact with the browser, and secondly it requires 100% of the CPU
time.

```cpp
// Avoid:
while (true) {
  result = IPC();
  // handle result
}
```

Rather sample the loop by sleeping long enough to relax the CPU usage, but short
enough to be responsive to new URLs:

```cpp
while (true) {
  result = IPC();
  // handle result
  sleep(T);
}
```

Given the fact that we need to be responsive to human interactions, the total
amount of time of one iteration of this loop shouldn't exceed the average human
reaction time. For example, if this time is 300 ms, then `T` is equal to 300 ms
minus the time to perform the IPC (e.g. 4 ms in average when using Scripting
Bridge API) minus the time to handle the result.

For example, if the IPC plus the result handling takes 20 ms in average, the CPU
usage is only 6.7% (20 ms of execution every 300 ms).

This can be an intermediary solution while finding and writing something
event-based.

Note that `T` could be dynamically computed. For example, when there is no more
user activity (no keyboard nor mouse events), `T` increases.

### Browser Extension

Create your own a browser extension to dispatch listen to the event internally
and dispatch it to your program.

### Indirect events

Maybe some indirect events can be listened, such as the keyboard and mouse
events, to perform the IPC when they occur (because they could lead to the event
we are polling).

### Logs

Does the browser write logs of opened URLs? If so, it could be used as event
source.

# Bibliography

1. [Chrome AppleScript Support](https://www.chromium.org/developers/design-documents/applescript)

Documentation:

1. [Event Architecture][cocoa-event-arch]

1. [Creating a Scripting Definition File][sdef]

1. [Chrome's SDEF][chrome-sdef]

1. [AppleScript: The Definitive Guide][applescript-book]

1. [Using Scripting Bridge][scripting-bridge]

1. [Improving Scripting Bridge Performances](https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ScriptingBridgeConcepts/ImproveScriptingBridgePerf/ImproveScriptingBridgePerf.html)

1. Apple Events Programming Guide

1. [Object Specifiers](https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ScriptableCocoaApplications/SApps_object_specifiers/SAppsObjectSpecifiers.html)

Programming:

1. [NSAppleEventDescriptor](https://developer.apple.com/documentation/foundation/nsappleeventdescriptor?language=objc)

1. [CreateObjSpecifier()](https://developer.apple.com/documentation/coreservices/1450244-createobjspecifier?language=objc)

1. [NSAppleScript](https://developer.apple.com/documentation/foundation/nsapplescript?language=objc)

1. [Apple Event Sending Modes](https://developer.apple.com/documentation/coreservices/aesendmode?language=objc)

[sdef]: https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ScriptableCocoaApplications/SApps_creating_sdef/SAppsCreateSdef.html

[cocoa-event-arch]: https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/EventOverview/EventArchitecture/EventArchitecture.html

[chrome-sdef]: https://chromium.googlesource.com/chromium/src/+/master/chrome/browser/ui/cocoa/applescript/scripting.sdef

[applescript-book]: https://books.google.fr/books?id=y6mbAgAAQBAJ

[scripting-bridge]: https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ScriptingBridgeConcepts/UsingScriptingBridge/UsingScriptingBridge.html
