@import Foundation;
@import AppKit;
#import "Chrome.h"

//
// Benchmarking helper macros
//
#define BENCHMARK_ITERATIONS 10000

#define BENCHMARK_LOOP_START()                                          \
  struct timespec ts;                                                   \
  struct {                                                              \
    unsigned long long min, max, avg;                                   \
  } benchmark_results = { .min = -1ULL, .max = 0, .avg = 0};            \
  for (int benchmark_iteration = 1;                                     \
       benchmark_iteration <= BENCHMARK_ITERATIONS;                     \
       ++benchmark_iteration) {                                         \
  const unsigned long long benchmark_start = time_ns(&ts);

#define BENCHMARK_LOOP_STOP()                                           \
  const unsigned long long benchmark_delta = time_ns(&ts) - benchmark_start; \
  BENCHMARK_RESULT_ADD(benchmark_iteration, benchmark_delta);           \
  }                                                                     \
  NSNumberFormatter* numberFormat = [[[NSNumberFormatter alloc] init] autorelease]; \
  numberFormat.usesGroupingSeparator = YES;                             \
  numberFormat.groupingSeparator = @",";                                \
  numberFormat.groupingSize = 3;                                        \
  NSString* minstr = [numberFormat stringFromNumber: [NSNumber numberWithInteger: benchmark_results.min]]; \
  NSString* avgstr = [numberFormat stringFromNumber: [NSNumber numberWithInteger: benchmark_results.avg]]; \
  NSString* maxstr = [numberFormat stringFromNumber: [NSNumber numberWithInteger: benchmark_results.max]]; \
  NSLog(@"%s min=%@ avg=%@ max=%@", __FUNCTION__, minstr, avgstr, maxstr);

#define BENCHMARK_RESULT_ADD(ITERATION, RESULT)                         \
  do {                                                                  \
    if (RESULT < benchmark_results.min)                                 \
      benchmark_results.min = RESULT;                                   \
    if (RESULT > benchmark_results.max)                                 \
      benchmark_results.max = RESULT;                                   \
    benchmark_results.avg = (RESULT + benchmark_results.avg * (ITERATION - 1)) / ITERATION; \
  } while (0)

static inline unsigned long long time_ns(struct timespec* const ts) {
  clock_gettime(CLOCK_REALTIME, ts);
  return ((unsigned long long) ts->tv_sec) * 1000000000ULL
         + (unsigned long long) ts->tv_nsec;
}

//
// ScriptingBridge API
//
// I check with LLDB that it calls AESendMessage() once, like every other
// methods used here.
//
void UsingScriptingBridgeAPI(void) {
  ChromeApplication *Chrome = [SBApplication applicationWithBundleIdentifier: @"com.google.Chrome"];
  SBElementArray* windows = [Chrome windows];
  NSArray* tabs = [windows arrayByApplyingSelector: @selector(tabs)];

  NSArray* urls;
  BENCHMARK_LOOP_START();
  // I checked with LLDB that this where the underlying Apple Event descriptor
  // is actually sent.
  urls = [tabs valueForKey: @"URL"];
  BENCHMARK_LOOP_STOP();

  // NSLog(@"%s result=%@", __FUNCTION__, urls);
}

//
// AppleScript API
//
void UsingAppleScriptAPI(void) {
  NSAppleScript* appScript = [[NSAppleScript alloc] initWithSource: @"tell application \"Google Chrome\" to get URL of tabs of windows"];
  [appScript compileAndReturnError: nil];

  NSAppleEventDescriptor* result;
  BENCHMARK_LOOP_START();
  result = [appScript executeAndReturnError: nil];
  BENCHMARK_LOOP_STOP();

  //NSLog(@"%s result=%@", __FUNCTION__, result);
}

//
// AppleEvent API
//
void UsingAppleEventAPI(void) {
  NSAppleEventDescriptor* target = [NSAppleEventDescriptor descriptorWithDescriptorType: typeApplicationBundleID
                                                                                   data: [@"com.google.Chrome" dataUsingEncoding:NSUTF8StringEncoding]];
  NSAppleEventDescriptor* event = [NSAppleEventDescriptor appleEventWithEventClass: 'core'
                                                                           eventID: 'getd'
                                                                  targetDescriptor: target
                                                                          returnID: -1
                                                                     transactionID: kAnyTransactionID];

  /*
    Attempt #1: using NS*Specifier seems to be for the server-side only. But
    getting their descriptor create the right AppleEvent descriptors, but empty.

  NSScriptClassDescription* containerClass = [NSScriptClassDescription classDescriptionForClass: [NSApplication class]];

  NSIndexSpecifier* windows = [[NSIndexSpecifier alloc] initWithContainerClassDescription: containerClass
                                                                       containerSpecifier: nil
                                                                                      key: @"appleScriptWindows"
                                                                                    index: -1];

  NSIndexSpecifier* tabs = [[NSIndexSpecifier alloc] initWithContainerSpecifier: windows
                                                                            key: @"tabs"];

  NSPropertySpecifier* url = [[NSPropertySpecifier alloc] initWithContainerSpecifier: tabs
                                                                                key: @"URL"];
  */

  /*
    Attempt #2: ends up with appearently good specifiers, but some types are in
    the end wrong: the following records should be chained as "Object
    Specifiers".

  NSAppleEventDescriptor* windowsRecord = [NSAppleEventDescriptor recordDescriptor];
  [windowsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'indx']
                forKeyword: 'form'];
  [windowsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'cwin']
                forKeyword: 'want'];
  [windowsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithDescriptorType: 'abso'
                                                                            bytes: " lla"
                                                                           length: 4]
                 forKeyword: 'seld'];
  [windowsRecord setDescriptor: [NSAppleEventDescriptor nullDescriptor]
                 forKeyword: 'from'];

  NSAppleEventDescriptor* tabsRecord = [NSAppleEventDescriptor recordDescriptor];
  [tabsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'indx']
                forKeyword: 'form'];
  [tabsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'CrTb']
                forKeyword: 'want'];
  [tabsRecord setDescriptor: [NSAppleEventDescriptor descriptorWithDescriptorType: 'abso'
                                                                            bytes: " lla"
                                                                           length: 4]
                 forKeyword: 'seld'];
  [tabsRecord setDescriptor: windowsRecord
                 forKeyword: 'from'];

  NSAppleEventDescriptor* urlRecord = [NSAppleEventDescriptor recordDescriptor];
  [urlRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'prop']
                forKeyword: 'form'];
  [urlRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'prop']
                forKeyword: 'want'];
  [urlRecord setDescriptor: [NSAppleEventDescriptor descriptorWithTypeCode: 'URL ']
                forKeyword: 'seld'];
  [urlRecord setDescriptor: tabsRecord
                forKeyword: 'from'];
  */

  // Attempt #3: thanks to LLDB, I could figure out that the records should be
  // wrapped in "Object Specifiers" (by comparing the layout of the descriptors
  // created by osascript with the descriptors I created with attempt #2), and I
  // finally found the `CreateObjSpecifier()` in an example of the book
  // "AppleScript: The Definitive Guide"...

  const AEDesc* all = [[NSAppleEventDescriptor descriptorWithDescriptorType: 'abso'
                                                                      bytes: " lla"
                                                                     length: 4] aeDesc];
  // Specifier for "all windows"
  AEDesc windows;
  CreateObjSpecifier('cwin',
                     (AEDesc*) [[NSAppleEventDescriptor nullDescriptor] aeDesc],
                     'indx',
                     (AEDesc*) all,
                     NO,
                     &windows);

  // Specifier for "all tabs of all windows"
  AEDesc tabs;
  CreateObjSpecifier('CrTb',
                     &windows,
                     'indx',
                     (AEDesc*) all,
                     NO,
                     &tabs);

  // Specifier for "URL of all tabs of all windows"
  AEDesc url;
  CreateObjSpecifier('prop',
                     &tabs,
                     'prop',
                     (AEDesc*) [[NSAppleEventDescriptor descriptorWithTypeCode: 'URL '] aeDesc],
                     NO,
                     &url);

  [event setParamDescriptor: [[NSAppleEventDescriptor alloc] initWithAEDescNoCopy: &url] forKeyword:keyDirectObject];

  NSAppleEventDescriptor* result;
  BENCHMARK_LOOP_START();
  result = [event sendEventWithOptions: NSAppleEventSendWaitForReply | NSAppleEventSendNeverInteract
                               timeout: -1
                                 error: nil];
  BENCHMARK_LOOP_STOP();

  //NSLog(@"%s result=%@", __FUNCTION__, result);
}

int main() {
  @autoreleasepool {
    UsingAppleEventAPI();
    UsingAppleScriptAPI();
    UsingScriptingBridgeAPI();
  }
  return 0;
}
